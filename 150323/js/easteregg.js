var secret = "717968";
var input = "";
var timer;
var mode = false;

$(document).keyup(function(e) {

    input += e.which;    
    
    clearTimeout(timer);
    timer = setTimeout(function() { input = ""; }, 200);
    
    check_input();
});

function check_input() {
    if(input == secret) {
        setStyle('matrix');
    }
}

$(document).ready(function() {
    setInterval(function() { $('#info').html('Keystroke: ' + input); }, 100);
});

