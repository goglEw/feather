$(document).ready(function() {	
	$(".menuopen").click(
	function() {
		$('.menu .searchopen').animate({ 'opacity': '1' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'opacity': '1' }, { duration: 100, queue: false } );
		$('.menu .searchopen').animate({ 'margin-right': '0px' }, { duration: 300 } );
		$('.menu .night, .menu .day').animate({ 'margin-right': '0px' }, { duration: 700 } );
		$('.menuopen').animate({ 'opacity': '0' } );
		$('.menuopen').css({ 'display': 'none' } );
		$('.menuclose').css({ 'display': 'block' } );
		$('.menuclose').animate({ 'opacity': '1' } );
	}
	);
	$(".menuclose").click(
	function() {
		$('.menu .searchopen').animate({ 'opacity': '0' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'opacity': '0' }, { duration: 100, queue: false } );
		$('.menu .searchopen').animate({ 'margin-right': '-100px' }, { duration: 300 } );
		$('.menu .night, .menu .day').animate({ 'margin-right': '-100px' }, { duration: 700 } );
		$('.menuclose').animate({ 'opacity': '0' } );
		$('.menuclose').css({ 'display': 'none' } );
		$('.menuopen').css({ 'display': 'block' } );
		$('.menuopen').animate({ 'opacity': '1' } );
	}
	);
	$(".searchopen").click(
	function() {
		$('.menuclose').animate({ 'opacity': '0' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'opacity': '0' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'right': '20px' }, { duration: 200 } );
		$('.menu .searchopen').animate({ 'right': '20px' }, { duration: 300 } );
		$('.menu .searchclose').css({ 'display': 'block' } );
		$('.menu .searchclose').animate({ 'opacity': '1' }, { duration: 300, queue: false } );
		$('.menu .searchopen').css({ 'display': 'none' } );
		$('.menu .night, .menu .day').css({ 'z-index': '0' } );
		$('input.search').css({ 'display': 'block' } );
		$('input.search').focus();
	}
	);
	$(".searchclose").click(
	function() {
		$('.menuclose').animate({ 'opacity': '1' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'opacity': '1' }, { duration: 200, queue: false } );
		$('.menu .night, .menu .day').animate({ 'right': '46px' }, { duration: 200 } );
		$('.menu .searchopen').animate({ 'right': '75px' }, { duration: 300 } );
		$('.menu .searchclose').css({ 'display': 'none' } );
		$('.menu .searchclose').animate({ 'opacity': '0' }, { duration: 300, queue: false } );
		$('.menu .searchopen').css({ 'display': 'block' } );
		$('.menu .night, .menu .day').css({ 'z-index': '1' } );
		$('input.search').css({ 'display': 'none' } );
	}
	);
})