<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>feather</title>
	    <meta name="author" content="Piet Terheyden">
	<meta http-equiv="cleartype" content="on">
	<LINK REL="SHORTCUT ICON" HREF="images/favicon.ico">

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="feather">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    
    <link rel="apple-touch-icon" href="images/icon120.png" sizes="120x120">
	
	<link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/animate.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/day.css" title="day" />
    <link rel="night stylesheet" type="text/css" href="css/night.css" title="night" />
    <link rel="matrix stylesheet" type="text/css" href="css/matrix.css" title="matrix" />
	
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/easteregg.js"></script>
<script type="text/javascript" src="js/styleswitcher.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script>
			window.addEventListener('load', function() {
				new FastClick(document.body);
			}, true);
        </script>
        <script>
            window.addEventListener('load', function() {
                document.body.addEventListener('touchmove', function(e) {
                    e.preventDefault();
                }, false);
            }, false);
        </script>
        <script>
        if (window.navigator.standalone) {
  $("meta[name='apple-mobile-web-app-status-bar-style']").remove();
}
</script>
<script type="text/javascript" src="js/autosave.js"></script>
<script type="text/javascript">
$(function(){
    $('#text').autosave({
        delay: 800,
        url: 'save.php'
    });
});
</script>
<script type="text/javascript">
$(function() {
    var text = $('#text').text();
    var charsLength = text.length;
    var wordsCount = text.split(' ').length;

    $('.counter').html('Words: ' + wordsCount + '&nbsp;&nbsp;Characters: ' + charsLength);
});
</script>
<script type="text/javascript">
$(function() {
    var data = $('#text').val();
    var space = '';
    $('textarea').focus().val('').val(data + space);
}); 
</script>
</head>
<body>
<div class="hidden">
 <img src="images/searchwhite.svg"/>
 <img src="images/searchblack.svg"/>
 <img src="images/menuclose.svg"/>
 <img src="images/menuclosewhite.svg"/>
 <img src="images/menuopen.svg"/>
 <img src="images/menuopen.svg"/>
 <img src="images/night.svg"/>
 <img src="images/day.svg"/>
</div>
<div id="matrix"> 
 <div class="t1" style="left:2px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t5" style="left:25px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t1" style="left:60px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t2" style="left:80px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t4" style="left:110px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t2" style="left:140px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t3" style="left:170px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t1" style="left:185px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t6" style="left:570px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t3" style="left:200px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t5" style="left:250px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t2" style="left:290px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t3" style="left:310px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t5" style="left:350px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t4" style="left:390px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t2" style="left:430px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t1" style="left:470px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t3" style="left:490px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t6" style="left:570px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t5" style="left:520px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
 <div class="t2" style="left:550px;">田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑</div>
</div> 
    <a class="menuopen" href="#" onclick="return false;"></a>
    <a class="menuclose" href="#" onclick="return false;"></a>
    <div class="menu">
	    <a href="#" class="searchopen" onclick="return false;"></a>
	    <a href="#" class="searchclose" onclick="return false;"></a>
	    <a href="#" class="day" onclick="setStyle('night'); return false;"></a>
	    <a href="#" class="night" onclick="setStyle('day'); return false;"></a>
    </div>
    <input autofocus class="search">
    <textarea id="text"><?=file_get_contents ('backup.txt');?></textarea>
    <div class="counter"></div>
</body>
</html>
