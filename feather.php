<?php
session_start();

include 'functions.php';
stillLoggedIn();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>feather</title>
  <meta name="author" content="Piet Terheyden">
  <meta http-equiv="cleartype" content="on">
  <link rel="shortcut icon" href="images/favicon.ico">

  <meta name="HandheldFriendly" content="True">
  <meta name="viewport" content="initial-scale=1.0, width=device-width, user-scalable=no">

  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="feather">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta http-equiv="cleartype" content="on">

  <link rel="apple-touch-icon" href="images/icon120.png" sizes="120x120">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/day.css" title="day" />
  <link rel="night stylesheet" type="text/css" href="css/night.css" title="night" />
  <link rel="matrix stylesheet" type="text/css" href="css/matrix.css" title="matrix" />
  <link rel="sheet stylesheet" type="text/css" href="css/sheet.css" title="sheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script type="text/javascript" src="js/shortcuts.js"></script>
  <script type="text/javascript" src="js/styleswitcher.js"></script>
  <script type="text/javascript" src="js/behave.js"></script>
  <script type="text/javascript" src="js/fullscreen.js"></script>
  <script type="text/javascript" src="js/menu.js"></script>
  <script>
    window.addEventListener('load', function() {
      new FastClick(document.body);
    }, true);
  </script>
  <script type="text/javascript" src="js/autosave.js"></script>
  <script type="text/javascript">
    $(function(){
      $('#text').autosave({
        delay: 200,
        url: 'save.php'
      });
    });
  </script>
  <script type="text/javascript">
    $(function() {
      var data = $('#text').val();
      var space = '';
      $('#text').focus().val('').val(data + space);
    });
  </script>
  <script>
    $(document).ready(function() {
      var text = document.querySelector("#text");

      $.ajax({
        url: 'load.php',
        success: function (response) {
          window.localStorage["TextEditorData"] = response;
          text.value = window.localStorage["TextEditorData"];
        }
      });

      text.addEventListener("keyup", function() {
        window.localStorage["TextEditorData"] = text.value;
      });
    });
  </script>
</head>

<body>

<div id="network_status">
  <span id="status"></span>
  <script>
    var statusElem = document.getElementById('status')

    setInterval(function () {
      statusElem.className = navigator.onLine ? 'online' : 'offline';
      statusElem.innerHTML = navigator.onLine ? '' : 'offline';
    }, 250);
  </script>
</div>

<a class="menuopen" href="#" onclick="return false;"></a>
<a class="menuclose" href="#" onclick="return false;"></a>

<div class="menu">
  <a href="#" class="sheet">Sheet</a>
  <a href="logout.php" class="logout"></a>
  <a href="#" class="fullscreen" onclick="return false;"></a>
  <a href="#" class="day" onclick="return false;"></a>
  <a href="#" class="night" onclick="return false;"></a>
</div>

<textarea id="text"></textarea>

<script>
var editor = new Behave({
    textarea: document.getElementById('text'),
    replaceTab: true,
    softTabs: false,
    tabSize: 4,
    autoOpen: true,
    overwrite: true,
    autoStrip: true,
    autoIndent: true,
    fence: false
});
</script>

<script>
$("textarea").bind("keyup", function() {
    var cursorPosition = $('textarea').prop("selectionStart");
    var text = $(this).val();
    if (text.indexOf('->') > -1) {
        text = text.replace(/->/g, "→");
        $(this).val(text);
        $('textarea').prop("selectionStart", cursorPosition - 1);
        $('textarea').prop("selectionEnd", cursorPosition - 1);
    }
});
</script>

</body>
</html>
