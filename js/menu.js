$(document).ready(function() {	
	$(".menuopen").click(
	function() {
    	$('.logout, .fullscreen, .menu .night, .menu .day').animate({ 'opacity': '1' }, { duration: 200, queue: false } );
		$('.menu').animate({ 'margin-right': '0px' }, { duration: 300 } );
		$('.menu .logout').animate({ 'margin-right': '0px' }, { duration: 400 } );
		$('.menu .fullscreen').animate({ 'margin-right': '0px' }, { duration: 500 } );
		$('.menu .night, .menu .day').animate({ 'margin-right': '0px' }, { duration: 600 } );
		$('.menuopen').animate({ 'opacity': '0' } );
		$('.menuopen').css({ 'display': 'none' } );
		$('.menuclose').css({ 'display': 'block' } );
		$('.menuclose').animate({ 'opacity': '1' } );
	}
	);
	$(".menuclose").click(
	function() {
		$('.logout, .fullscreen, .menu .night, .menu .day').animate({ 'opacity': '0' }, { duration: 100, queue: false } );
		$('.menu').animate({ 'margin-right': '-130px' }, { duration: 300 } );
		$('.menu .logout').animate({ 'margin-right': '-120px' }, { duration: 300 } );
		$('.menu .fullscreen').animate({ 'margin-right': '-100px' }, { duration: 300 } );
		$('.menu .night, .menu .day').animate({ 'margin-right': '-100px' }, { duration: 700 } );
		$('.menuclose').animate({ 'opacity': '0' } );
		$('.menuclose').css({ 'display': 'none' } );
		$('.menuopen').css({ 'display': 'block' } );
		$('.menuopen').animate({ 'opacity': '1' } );
	}
	);
	$(".fullscreen").click(
	function() {
		screenfull.toggle($('#container')[0]);
	}
	);
	$(".day").click(
	function() {
		setStyle('night');
	}
	);
	$(".night").click(
	function() {
		setStyle('day');
	}
	);
	$(".sheet").click(function() {
	  $("textarea").toggleClass("sheetstyles");
	});
})