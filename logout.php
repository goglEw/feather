<?php
     session_start();
     session_destroy();

     include 'url.php';

     header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.html');
?>