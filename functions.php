<?php

function stillLoggedIn() {
    session_start();

    include 'url.php';

    if (!isset($_SESSION['angemeldet']) || !$_SESSION['angemeldet']) {
        header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/index.html');
        exit;
    }
}


?>